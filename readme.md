# nodeto3d

The following module allows for generation of 3d objects/.stl files created from directed graphs using the networkx library. 

## Getting Started

### Prerequisites

All requirements are found in requirements.txt.

```
pip install requirements.txt
```

### Installing

Please make sure you have the correct version of networkx installed.

## Example generation of directed graph

We are going to create a directed graph, customise it, and then convert it to a 3d .stl format, which we can then 3d print!

### Creating a directed graph

An example ('./example/example.dot') has been provided, as well as an accompanying python file in the same directory to show how a directed graph can be simply generated. For our example, we have created the following graph:

![digraph](https://bitbucket.org/janlietava/nodeto3d/raw/b75263c59ef814623aa841dc1fa3aa39f24c1451/example/images/digraph_example_w500.PNG)

This digraph can be simply obtaibned by loading the .dot file as follows:

```
G = read_dot('./example/example.dot')
```

Alternatively, one can generate the example from scratch as seen in ./examples/example_directed_tree.py.

### Altering the directed graph layout to a radial structure

To visualise the directed graph in 3d space, we first want to edit the hierarchical structure to be radial. This can be achieved using:
 

```
m = MeshHandler(G)
m.generate_radial_position()
nx.draw(G, pos=m.pos, with_labels=True,node_size = 50)
```

This will provide the following output:

![digraphradial](https://bitbucket.org/janlietava/nodeto3d/raw/b75263c59ef814623aa841dc1fa3aa39f24c1451/example/images/digraph_example_radial_w500.PNG)

### Generating an object structure to represent the radial digraph in 3d space

We can then convert the radial digraph into 3d space:

```
m.generate_object_structure(node_res = 45, node_size = 1.15, edge_res = 35, edge_size = 0.6)

m.plot_object_structure()
```

Which looks like this:

![digraphradial_objects](https://bitbucket.org/janlietava/nodeto3d/raw/b75263c59ef814623aa841dc1fa3aa39f24c1451/example/images/digraph_example_object_structure_w500.PNG)

### Output and prepare our 3d structure for 3d printing (or just visualising)

We can now convert the object structure into .stl format using the following code:

```
m.generate_delaunay_mesh()
```

We use delayney triangulation to convert our edges and nodes into .stl format. These separate edges and nodes can be loaded into any software which supports .stl, such as blender, where they can be joined and prepared for potential 3d printing, as seen below:

![digraphradial_objects](https://bitbucket.org/janlietava/nodeto3d/raw/b75263c59ef814623aa841dc1fa3aa39f24c1451/example/images/digraph_example_stls_w500.PNG)


### 3d printing our object

We can now use any 3d printing software to prepare the .stl for printing. In the following example, Cura was used to generate the .gcode from the above example, and it was printed using a Creality Ender 3.

![digraphradial_3d_printed](https://bitbucket.org/janlietava/nodeto3d/raw/d7b38a01827e74d668b617873c190a7dc5e6713c/example/images/digraph_example_3dprinted_w500.PNG)
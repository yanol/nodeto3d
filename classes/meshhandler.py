import numpy
from stl import mesh
import scipy
from scipy.spatial import ConvexHull
import matplotlib.tri as mtri
import numpy as np
import matplotlib.cm as cm
from scipy.spatial import Delaunay
import math
import networkx as nx
from networkx.drawing.nx_agraph import write_dot, graphviz_layout, read_dot
from tqdm import tqdm
import os

from classes.sphere import sphere
from classes.cylinder import cylinder

class MeshHandler:
    
    def __init__(self, G):
        self.G = G
        self.pos = graphviz_layout(G, prog='dot')
        self.object_structure = {'nodes':[], 'edges':[]}
        self.mesh = None
        
    def generate_delaunay_mesh(self, object_structure = None, name = 'current_tree', output = './output/'):
        
        if not object_structure:
            object_structure = self.object_structure
            
        if not os.path.isdir('./output/'+str(name)):
            os.mkdir("./output/"+str(name))
            
        node_tree = []
        
        for object_type, object_list in object_structure.items():
            node_tree = node_tree + object_list
            
        for i, o in enumerate(tqdm(node_tree, position=0, leave=True)):
    
            o.create_stl_file('node_tree_part_'+str(i))
        
    def plot_object_structure(self):
        
        fig = plt.figure()
        
        ax = fig.add_subplot(111, projection = '3d')
        
        ax.set_xlim([-10,10])
        ax.set_ylim([-10,10])
        ax.set_zlim([-10,10])
        
        for _ol in list(self.object_structure.values()):
            for _o in _ol:
                _o.plot_self(ax)
        
    def generate_object_structure(self, G = None, node_res = 45, node_size = 0.25, edge_res = 35, edge_size = 0.1):
        
        if not G:
            G = self.G
            
        nds = G.nodes()
        
        for i, (k,v) in enumerate(tqdm(self.pos.items(), position=0, leave=True)):
    
            _level = float(nds[k]['_level'])
            t = (G.predecessors(k))

            successors = G.successors(k)

            a = np.array([v[1], v[0], _level*3])
            s = sphere(a[0], a[1], a[2], node_size, resolution = node_res)
            self.object_structure['nodes'].append(s)

            for s in successors:

                parent_node = self.pos[s]
                parent_node_level = float(nds[s]['_level'])

                b = np.array([parent_node[1], parent_node[0], parent_node_level*3])

                magbvec = b-a
                magb = np.sqrt(magbvec.dot(magbvec))

                cylinder_object = cylinder(a[0], a[1], a[2], radius = edge_size, z=magb, resolution = edge_res)
                cylinder_object.rotate_cylinder_a2b(a, b)
                self.object_structure['edges'].append(cylinder_object)
        
    def generate_radial_position(self, G = None, root = 'ROOT', width = 2.*math.pi, vert_gap = 2.0, vert_loc = 0, xcenter = 0):
        
        if not G:
            G = self.G
        
        self.pos = self.radial_pos(self.hierarchy_pos(G, root, width, vert_gap, vert_loc, xcenter))
        
    def radial_pos(self, h_G):
        return {u:(r*math.cos(theta),r*math.sin(theta)) for u, (theta, r) in h_G.items()}
        
    def hierarchy_pos(self, G, root=None, width=1., vert_gap = 0.2, vert_loc = 0, xcenter = 0.5):

        '''
        From https://stackoverflow.com/questions/29586520/can-one-get-hierarchical-graphs-from-networkx-with-python-3/29597209#29597209
        From Joel's answer at https://stackoverflow.com/a/29597209/2966723 

        If the graph is a tree this will return the positions to plot this in a 
        hierarchical layout.

        G: the graph (must be a tree)

        root: the root node of current branch 
        - if the tree is directed and this is not given, the root will be found and used
        - if the tree is directed and this is given, then the positions will be just for the descendants of this node.
        - if the tree is undirected and not given, then a random choice will be used.

        width: horizontal space allocated for this branch - avoids overlap with other branches

        vert_gap: gap between levels of hierarchy

        vert_loc: vertical location of root

        xcenter: horizontal location of root
        '''
        #if not nx.is_tree(G):
        #    raise TypeError('cannot use hierarchy_pos on a graph that is not a tree')

        if root is None:
            if isinstance(G, nx.DiGraph):
                root = next(iter(nx.topological_sort(G)))  #allows back compatibility with nx version 1.11
            else:
                root = random.choice(list(G.nodes))

        def _hierarchy_pos(G, root, width=1., vert_gap = 0.2, vert_loc = 0, xcenter = 0.5, pos = None, parent = None):
            '''
            see hierarchy_pos docstring for most arguments

            pos: a dict saying where all nodes go if they have been assigned
            parent: parent of this branch. - only affects it if non-directed

            '''

            if pos is None:
                pos = {root:(xcenter,vert_loc)}
            else:
                pos[root] = (xcenter, vert_loc)
            children = list(G.neighbors(root))
            if not isinstance(G, nx.DiGraph) and parent is not None:
                children.remove(parent)  
            if len(children)!=0:
                dx = width/len(children) 
                nextx = xcenter - width/2 - dx/2
                for child in children:
                    nextx += dx
                    pos = _hierarchy_pos(G,child, width = dx, vert_gap = vert_gap, 
                                        vert_loc = vert_loc-vert_gap, xcenter=nextx,
                                        pos=pos, parent = root)
            return pos


        return _hierarchy_pos(G, root, width, vert_gap, vert_loc, xcenter)
    
    def generate_organic_structure(self, points = None, distance_threshold = 4, is_funnel = False, is_normal_funnel = False, is_spinning_top = False, node_size = 0.35, node_resolution = 30, edge_size = 0.32, edge_resolution = 40):
        
        def generate_sphere_density(n_p, radius = 10.0):
            return np.random.randint(radius, size=(3, n_p))   
        
        def compare_edges(edgelist, e1, e2):
            for e in edgelist:
                if e == [e1, e2] or e == [e2, e1]:
                    return False
            return True
        
        g = points
        
        if not points:
            g = generate_sphere_density(8,4)
        
        edges =  []

        for i, (x,y,z) in enumerate(zip(g[0],g[1],g[2])):

            s = sphere(x,y,z ,node_size, resolution=node_resolution)
            self.object_structure['nodes'].append(s)

            for i2, (x2,y2,z2) in enumerate(zip(g[0],g[1],g[2])):

                dist = np.sqrt((x2-x)**2 + (y2-y)**2 + (z2-z)**2)

                if dist != 0 and dist < distance_threshold:

                    e1, e2 = [x,y,z], [x2,y2,z2]

                    if compare_edges(edges, e1, e2):

                        cylinder_object = cylinder(x,y,z, radius=edge_size, z=dist, resolution=edge_resolution, is_normal_funnel = is_normal_funnel)
                        cylinder_object.rotate_cylinder_a2b(np.array([x,y,z]), np.array([x2,y2,z2]))

                        edges.append([[x,y,z], [x2,y2,z2]])
                        
                        self.object_structure['nodes'].append(cylinder_object)
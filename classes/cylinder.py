#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy
from stl import mesh
import scipy
from scipy.spatial import ConvexHull
import matplotlib.tri as mtri
from scipy.spatial import Delaunay

class cylinder(object):

    def __init__(self, center_x = 0.0, center_y = 0.0, center_z = 0.0, radius = 1.0, z = 1.0, resolution = 20, is_funnel = False, is_spinning_top = False, is_normal_funnel = False):

        self.center_x = center_x
        self.center_y = center_y
        self.center_z = center_z
        self.radius = radius
        self.z = z
        self.resolution = resolution
        self.cylinder = self._define_cylinder(is_funnel = is_funnel, is_spinning_top = is_spinning_top, is_normal_funnel = is_normal_funnel)
        self.rotated_cylinder = None

    def _print_cylinder(self):
        print (self.cylinder)
        
    def _define_cylinder(self, is_funnel = False, is_spinning_top = False, is_normal_funnel = False):

        self.phi = np.linspace(0, 2*np.pi, self.resolution)
        self.z_values = np.linspace(0, self.z, self.resolution)

        self.theta_mesh, self.z_grid = np.meshgrid(self.phi, self.z_values)
        
        if is_spinning_top:            
            normalised_z_grid = 1.0/(abs(self.z_grid - np.amax(self.z_grid)/2.0)) + 0.3
            
        elif is_funnel:   
            #normalised_z_grid = 1.0/(np.amax(self.z_grid)-abs(self.z_grid - np.amax(self.z_grid)/2.0))
            
            symmetrical_z_grid = abs(self.z_grid - np.amax(self.z_grid)/2.0)
            symmetrical_z_grid = abs(1./np.log(symmetrical_z_grid + 1))
            
            #print symmetrical_z_grid
            
            normalised_z_grid = (symmetrical_z_grid*(1./np.amax(symmetrical_z_grid)))
            
            #print (normalised_z_grid)
            
        elif is_normal_funnel:
            normalised_z_grid = 1.0/(np.amax(self.z_grid)-abs(self.z_grid - np.amax(self.z_grid)/2.0))
            normalised_z_grid = (normalised_z_grid*(1./np.amax(normalised_z_grid)))
        else:
            normalised_z_grid = 1.0
       
    
        #print normalised_z_grid
        self.x_grid = normalised_z_grid * self.radius * np.cos(self.theta_mesh) + self.center_x
        self.y_grid = normalised_z_grid * self.radius * np.sin(self.theta_mesh) + self.center_y
        self.z_grid = self.z_grid + self.center_z
        
        #self.x_grid = self.radius * np.cos(self.phi) + self.center_x
        #self.y_grid = self.radius * np.sin(self.phi) + self.center_y
        #self.z_grid = self.z_values + self.center_z

        return [self.x_grid, self.y_grid, self.z_grid]

    def rotate_cylinder_a2b(self, a, b):
        
        b_prime = b-a
        cylinder = np.array([0,0,1.0])
        
        #print (b_prime)
        self.calculate_rotation_matrix(cylinder, b_prime)
        
        rotate_matrix = self.rotation_matrix
        #print (rotate_matrix)
        
        x_list = self.x_grid.ravel()
        y_list = self.y_grid.ravel()
        z_list = self.z_grid.ravel()
        
        x_rotated = []
        y_rotated = []
        z_rotated = []
        
        for (x,y,z) in zip(x_list, y_list, z_list):
            current_vector = np.array([x,y,z])-a
            rotated_vector = np.dot(rotate_matrix, current_vector)
            
            x_rotated.append(rotated_vector[0] + a[0])
            y_rotated.append(rotated_vector[1] + a[1])
            z_rotated.append(rotated_vector[2] + a[2])
        
        #print (x_rotated, y_rotated, z_rotated)
        
        self.x_grid = np.array(x_rotated).reshape(self.x_grid.shape)
        self.y_grid = np.array(y_rotated).reshape(self.y_grid.shape)
        self.z_grid = np.array(z_rotated).reshape(self.z_grid.shape)
        
        #print (self.x_grid_rotated, self.y_grid_rotated, self.z_grid_rotated)
        self.rotated_cylinder = [self.x_grid, self.y_grid, self.z_grid]
        #self.rotate_cylinder(rotate_matrix)
    
    def calculate_rotation_matrix(self, a, b):
        
        a = np.array(a)
        b = np.array(b)
        
        mag_b = np.sqrt(b.dot(b))
        mag_a = np.sqrt(a.dot(a))
        
        #print (a,b,mag_b,mag_a)
        
        if mag_a != 0.0:
            #print 'Found non-0 vector!'
            a = a/mag_a
            
        b = b/mag_b
                
        v = np.cross(a,b)
        c = a.dot(b)
            
        self.v_x = np.array([[0, -v[2], v[1]], 
                             [v[2], 0, -v[0]], 
                             [-v[1], v[0], 0]])
        
        self.v_x2 = self.v_x.dot(self.v_x)
        
        i = np.identity(3)
        
        self.rotation_matrix = i + self.v_x + (self.v_x2*(1./(1+c)))
        
    def rotate_cylinder(self, rotation_matrix):
        
        rotation = np.dot(self.rotation_matrix, np.array([self.x_grid.ravel(), self.y_grid.ravel(), self.z_grid.ravel()]))
        
        x,y,z = self.x_grid, self.y_grid, self.z_grid
        
        self.x_grid = rotation[0,:].reshape(x.shape) 
        self.y_grid = rotation[1,:].reshape(y.shape)           
        self.z_grid = rotation[2,:].reshape(z.shape)
    
    def join_points_euler(self, x1, y1, z1, x2, y2, z2):

        dx = x1 - x2
        dy = y1 - y2
        dz = z1 - z2

        distance = math.sqrt(dx**2 + dy**2 + dz**2)
        
        self.z = distance
        self._define_cylinder()

        self.psi = math.atan2(dy, -dz)
        self.theta = math.atan2(dx, dy)
        self.phi = 0

        self.euler = np.array([[(math.cos(self.psi)*math.cos(self.phi)) - math.cos(self.theta)*math.sin(self.phi)*math.sin(self.psi), math.cos(self.psi)*math.sin(self.phi) + math.cos(self.theta)*math.cos(self.phi)*math.sin(self.psi), math.sin(self.psi)*math.sin(self.theta)],
                               [-math.sin(self.psi)*math.cos(self.phi) - math.cos(self.theta)*math.sin(self.phi)*math.cos(self.psi), -math.sin(self.psi)*math.sin(self.phi) + math.cos(self.theta)*math.cos(self.phi)*math.cos(self.psi), math.cos(self.psi)*math.sin(self.theta)],
                               [math.sin(self.theta)*math.sin(self.phi), -math.sin(self.theta)*math.cos(self.phi), math.cos(self.theta)]])

        rotation = np.dot(self.euler, np.array([self.x_grid.ravel(), self.y_grid.ravel(), self.z_grid.ravel()]))

        x,y,z = self.x_grid, self.y_grid, self.z_grid

        self.x_grid = rotation[0,:].reshape(x.shape)               
        self.y_grid = rotation[1,:].reshape(y.shape)               
        self.z_grid = rotation[2,:].reshape(z.shape)

    def plot_self(self, ax):

        ax.plot_surface(self.x_grid, self.y_grid, self.z_grid)
        
    def return_coordinates(self):
        if self.rotated_cylinder:
            return self.rotated_cylinder
        return self.cylinder
    
    def create_stl_file(self, name, directory = './output/', project_name = 'current_tree'):
        
        xyz = self.return_coordinates()
        x,y,z = xyz[0].flatten(),xyz[1].flatten(),xyz[2].flatten()

        u,v = self.return_parametric()

        points2D = np.vstack([u,v]).T
        tri = Delaunay(points2D)
        simplices = tri.simplices

        tri = mtri.Triangulation(x, y, triangles=simplices)

        data = np.zeros(len(tri.triangles), dtype=mesh.Mesh.dtype)
        mobius_mesh = mesh.Mesh(data, remove_empty_areas=False)
        mobius_mesh.x[:] = x[tri.triangles]
        mobius_mesh.y[:] = y[tri.triangles]
        mobius_mesh.z[:] = z[tri.triangles]
        mobius_mesh.save(directory + project_name + '/' + name + '.stl')
    
    def return_parametric(self):
        return [self.theta_mesh.flatten(), self.z_grid.flatten()]
    
    def return_type(self):
        return 'cylinder'
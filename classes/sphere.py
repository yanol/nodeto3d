#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy
from stl import mesh
import scipy
from scipy.spatial import ConvexHull
import matplotlib.tri as mtri
from scipy.spatial import Delaunay

class sphere(object):

    def __init__(self, center_x = 0.0, center_y = 0.0, center_z =0.0, radius = 1.0, resolution = 20):

        self.center_x = center_x
        self.center_y = center_y
        self.center_z = center_z
        self.radius = radius
        self.resolution = resolution   
        self.sphere = self._define_sphere()

    def _define_sphere(self):

        self.u, self.v = np.mgrid[0:2*np.pi: (self.resolution * 1j), 0:np.pi: (self.resolution * 1j)]
        self.x = self.radius * np.cos(self.u)*np.sin(self.v) + self.center_x
        self.y = self.radius * np.sin(self.u)*np.sin(self.v) + self.center_y
        self.z = self.radius * np.cos(self.v) + self.center_z

        return [self.x, self.y, self.z]

    def plot_self(self, ax):

        ax.plot_surface(self.x, self.y, self.z)
        
    def return_coordinates(self):
        return self.sphere
    
    def return_parametric(self):
        return [self.u.flatten(), self.v.flatten()]
    
    def return_type(self):
        return 'sphere'

    def create_stl_file(self, name, directory = './output/', project_name = 'current_tree'):
        
        xyz = self.return_coordinates()
        x,y,z = xyz[0].flatten(),xyz[1].flatten(),xyz[2].flatten()

        u,v = self.return_parametric()

        points2D = np.vstack([u,v]).T
        tri = Delaunay(points2D)
        simplices = tri.simplices

        tri = mtri.Triangulation(x, y, triangles=simplices)

        data = np.zeros(len(tri.triangles), dtype=mesh.Mesh.dtype)
        mobius_mesh = mesh.Mesh(data, remove_empty_areas=False)
        mobius_mesh.x[:] = x[tri.triangles]
        mobius_mesh.y[:] = y[tri.triangles]
        mobius_mesh.z[:] = z[tri.triangles]
        mobius_mesh.save(directory + project_name + '/' + name + '.stl')
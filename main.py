#!/usr/bin/env python

from networkx.drawing.nx_agraph import write_dot, graphviz_layout, read_dot
from classes.meshhandler import MeshHandler

if __name__=="__main__":

    G = read_dot('./example/example.dot')
    
    m = MeshHandler(G)

    m.generate_radial_position()
    
    m.generate_object_structure(node_res = 45, node_size = 1.15, edge_res = 35, edge_size = 0.6)

    m.generate_delaunay_mesh()
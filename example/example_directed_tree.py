import matplotlib.pyplot as plt
import networkx as nx
from networkx.drawing.nx_agraph import write_dot, graphviz_layout, read_dot
import math
import random
import numpy as np

G = nx.DiGraph()

plt.figure()

G.add_node("ROOT", _level = 0.0)

for i in range(3):
    G.add_node("Child_%i" % i, _level = -1.0)
    G.add_node("Grandchild_%i" % i, _level = -2.0)
    G.add_node("Greatgrandchild_%i" % i, _level = -3.0)

    G.add_edge("ROOT", "Child_%i" % i)
    G.add_edge("Child_%i" % i, "Grandchild_%i" % i)
    G.add_edge("Grandchild_%i" % i, "Greatgrandchild_%i" % i)

G.add_node("Greatgrandchild_3", _level = -3.0)
G.add_edge("Grandchild_2", "Greatgrandchild_3")

G.add_node("Greatgrandchild_4", _level = -3.0)
G.add_edge("Grandchild_2", "Greatgrandchild_4")
    
# same layout using matplotlib with no labels
plt.title('draw_networkx')
pos = graphviz_layout(G, prog='dot')

nx.draw(G, pos=pos, with_labels=True, arrows=True)
plt.plot()